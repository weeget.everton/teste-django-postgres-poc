# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='testedj',
    version='1.1.6',
    description='OpenShift App',
    author='WeeGet',
    author_email='weeget.br@gmail.com',
    url='http://www.python.org/sigs/distutils-sig/',
# GETTING-STARTED: define required django version:
    install_requires=[
        'Django==1.8.4'
    ],
    package_dir={'': 'src'},
)
