# WeeGet Academy

# Indice

1. Infra
2. Instalaçao
3. Instalando e Configurando o Banco de Dados
4. Ambiente de Desenvolvimento
5. Arquivos Estáticos
6. Verificando e Validando os Migrations
7. Rodando o Projeto Localmente
8. Deploy
9. Openshift

## 1- Infra
    *Versão do python: 3.3.2*
    Versão do Django: 1.8
    Versão do Postgres: 9.2

## 2- Instalação
### Criando e Aplicando sua chave public (SSH) no gitlab
Para fazer pull e pushs através de SSH é necessário criar 
uma chave publica(caso nao tenha) e adicionar ao repositorio
do git lab

Para verificar a existencia de uma SSH key

    cat ~/.ssh/id_rsa.pub
    
Se aparecer algem parecido com o codigo baixo, essa é a sua
ssh key, caso ja existe pule para o **`passo 2!!`**

    ssh-rsa AAAAB3NzaC1yc2...
    
1 - Gerando uma nova SSH Key (Caso não possua uma)

    ssh-keygen -t rsa -C "your.email@example.com" -b 4096

2 - Copiando a SSH Key e salvando no gitlab

`Uso de uma ferramenta para copiar o ssh key é
mais como uma prevensão de copia humana errada da 
key`

**2.1 - Ubuntu**

2.1.1 - Instalando xclip para copiar para o clipboard
    
    sudo apt-get install xclip
    
2.1.2 - Copiando a ssh para o clipboard com o xclip
    
    xclip -sel clip < ~/.ssh/id_rsa.pub
    
**2.2 - macOS**

2.2.1 - Copiando a ssh para o clipboard

    pbcopy < ~/.ssh/id_rsa.pub

3 - Vinculando sua SSH Key ao Gitlab

Com sua conta logada (sua conta de desenvolvedor, que voce
irá realizar as operações no gitlab) acesse o link 
`https://gitlab.com/profile/keys`

No box superior você pode colar (**ctrl + v**) a sua ssh key e 
abaixo em titulo você pode dar um nome para identificar a sua ssh.
Exemplo: diego-notebook-lenovo

Na lista abaixo aparece a lista das suas ssh keys cadastradas na sua
conta, **`cada computador que você precise baixar ou alterar o projeto
será necessária gerar e adicionar a ssh dessa maquina ao gitlab
ou o usuário ficará incapaz de interagir com o projeto`**

### Instalando o Git

    sudo apt-get install git-core
    
Para verificar se o git foi devidamente instalado rode o seguinte comando

    git --version
    
Se aparecer uma numeração indicando a versão, então o mesmo foi devidamente
instalado

### Baixando o projeto weetget-nuvem

    git clone git@gitlab.com:weeget/weeget-nuvem.git
    
### Linux (Ubuntu)

    sudo apt-get install python3
    
### macOS

    brew install python3

### Instalando e Configurando Virtual machine

    sudo apt-get install virtualenv
    virtualenv -p python3 vm-weeget

*`Caso use multiplas versões com o pyenv rode da seguinte forma:`*
Mais duvidas chegar na documentação do pyenv para auxilio na instalação de
multiplas versões do python na maquina

    virtualenv -p ~/.pyenv/versions/3.3.2/bin/python3.3 vm-weeget
    
### Ativando a Virtual Env

A virtual env serve para encapsular as libs necessarias do projetos, dessa forma
é possivel possuir mais de um projeto sem ter problemas de dependencias ou
conflitos

    source vm-weeget/bin/activate
    
No seu terminal irá aparecer (vm-weeget) no começo das linhas que você digitar,
isso indica que a VM está ativa nesse terminal, tudo que for instalado através
do pip ou pip3 será instalado na vm em vez de instalar globalmente na maquina
    
### Instalando as dependencias do projeto
Acesso a pasta raiz do projeto django (voce pode identificar
ela pela presença do arquivo manage.py)

    cd weeget-nuvem/src/weeget
    
O projeto depende de algumas libs externas (precisam ser instaladas
globalmente para o funcionamento do sistema)

**1 - Ubuntu**

    sudo apt-get install libffi libffi-dev
    sudo apt-get install libncurses5-dev
    
Caso não encontre o libffi no apt-get, pode seguir os seguintes passos:

    1) Acesse o source da biblioteca no site do Launchpad de acordo com sua Distro (ex: caso use Ubuntu 16.04 (Xenial Xerus), acesse https://launchpad.net/ubuntu/xenial/+source/libffi)

    2) Baixe o source da biblioteca (libffi_3.2.1.orig.tar.gz)

    3) Extraia o arquivo e abra um terminal na pasta extraída

    4) De preferência, leia o README para saber os passos para instalação, mas em resumo, são estes passos:

    4.1) Rode o comando:
    
    ./configure
    
    4.2) Após, rode o comando:
    
    ./make
    
    4.3) Caso não tenha instalado, instale o DejaGNU (ex: sudo apt-get install dejagnu) e rode o comando:
    
    ./make check
    
    4.4) Finalmente, instale a biblioteca com o comando:
    
    sudo make install

    
**2 - macOS**

    brew install cairo pango gdk-pixbuf libxml2 libxslt libffi
    
Nessa pasta existem 2 arquivos de requirements, um deles é o padrão 
(requirements.txt)
para que todas as funcionalidades do projeto de fato funcione
 e o segundo (requirements-dev.txt) é para instalar as features
 de dev, como debug bar e etc, o que irá facilitar o desenvolvimento
 
 **`Jamais deverá instalar os requirements de dev no ambiente de produção
 ou de dev remoto, apenas localmente`**
 
 `Antiga versão: Será descontinuada`

    pip3 install -r requirements.txt
    pip3 install -r requirements-dev.txt

*Nova versão*

    pip3 install -r requirements/dev.txt
    
**'Toda vez que for adicionada uma nova dependencia ao projeto é extritamente
obrigatório adicionar a mesma no requirements, identificando se a mesma
é de propriedade de produção (necessaria para alguma feature do sistema
funcione) ou de propriedade de desenvolvimento (alguma ferramenta de auxilio
ao desenvolvimento)'**

## 3 - Instalando e Configurando o Banco de Dados
Para melhor simular o ambiente de produção localmente para realizar
testes iremos configurar um banco local como espelho do banco de
produção

`Como os dados que estão no servidor de produção agora 
tambem estarão na sua maquina local, cuidado com exposição dos
dados`

1 - Instalando postgres sql localmente

    sudo apt-get update
    sudo apt-get install postgresql postgresql-contrib pgadmin3
    
2 - Configurando banco local
**Toda configuração pode ser feita de forma manual e visual
atráves do pgadmin3 o que é mais facil, abaixo será mostrado
como fazer através linha de comando
    
Agora iremos criar uma nova role (usuario)

    createuser --interactive

Nesse momento será solicitado o nome da role (usuario) e se ela
terá os privilegios de superusuario

Nesse momento preencha com o nome weeget, e sete como superusuario

    Output
    Enter name of role to add: weeget
    Shall the new role be a superuser? (y/n) y

Agora será necessário conectar na interface do banco de dados
para alterar a senha do usuário weeget criado anteriormente

    sudo -u postgres psql
    
Vamos criar uma senha `weeget` para esse usuario

    ALTER USER weeget WITH PASSWORD 'weeget'";

Para sair da interface do banco de dados e voltar para o terminal
    
    \q
    
Agora que temos nosso usuario weeget criado iremos criar o banco
de dados

    createdb weeget
    
Agora devemos usar o arquivo chamado `master.backup.sql' encontrado
na pasta `weeget-nuvem/database' navegue até essa pasta

    cd weeget-nuvem/database
    
Rode o script para popular o banco de dados

    psql -U weeget -W weeget -h 127.0.0.1 -p 5432 -d weeget < master.backup.sql
    
`Nesse momento pode aparecer alguns erros, mas é natural pois o script
as vezes tentar criar algo que ja existe então ele acusa um pequeno
erro e continua`

Pronto agora temos o banco devidamente configurado localmente e com
os seus dados povoados
    
## 4 - Ambiente de Desenvolvimento

Agora que temos nosso projeto com suas dependencias ja instaladas
e o banco de dados ja configurado e povoados, podemos fazer algumas
configurações basicas no projeto para de fato podermos roda-lo

Na estrutura do projeto possuimos varios arquivos de configuração
um para cada ambiente, e eles possuem informações e configurações
pertinentes a esse ambiente, logo não teremos todas as informações
que alguns deles precisam para execução, logo teremos que **indicar
que queremos utilizar a configuração local**

Para fazer nosso sistema rodar as configurações locais devemos
criar uma variavel de ambiente e setar um valor especifico nela

Para criar essa variavel existem 2 formas, a primeira que irei 
ensinar é a forma mais rapida e ela apenas ira funcionar na instancia
(janela) do terminal que voce está usando no momento

Ja a segunda irá persistir, logo o processo não deverá ser refeito a
todo momento

**1 - Primeira Forma (Simples e repetitiva)**

No terminal digite

    export ENVIRONMENT=local
    
Para verificar se funcionou digite o seguinte comando

    echo $ENVIRONMENT
    
O resultado deverá ser `local`
    
**2 - Segunda Forma (Perigosa e definitiva)**
`Muita atenção ao executar essa forma, pois o arquivo que irá
alterar é muito importante para o seu sistema operacional`

    cp /etc/bash.bashrc $HOME/.bashrc 
    
Adicione a seguinte linha **ao final do arquivo**

    export ENVIRONMENT=local
    
Agora carregue a variavel que você acabou de adicionar

    source $HOME/.bashrc
    
## 5 - Arquivos estaticos
TODO (A ser feito... verificar como é realizado a integração dos estaticos
no S3 da Amazon)
    
## 6 - Verificando e Validando os Migrations

Os migrations são arquivos de configuração do django onde eles pegam as 
configurações da classe e vinculam ao banco criando tabelas, relações e etc...

É necessário rodar esse comando para verificar se as classes
do projeto está condizendo com o banco que criamos

**Na raiz do projeto django (onde se encontra o arquivo manage.py)
`weeget-nuvem/src/weeget/`**

    python manage.py migrate
    
Toda alteração realizada nas estruturas das classes (criação ou
remoção de atributos devesse rodar o comando abaixo)

    python manage.py makemigrations
    
O Django cria arquivos de migrations para sincronizar a estrutura
do projeto com o banco de dados, o mando `makemigrations`cria
essas migrations e o comando `migrate`aplica essas alterações no 
banco
    
## 7 - Rodando o Projeto Localmente

Para rodar o projeto primeiro devemos criar o arquivo de CACHE
do forum **SPIRIT** utilizado no projeto (esse processo so precisa
ser realizado uma vez)

    python manage.py createcachetable
    
Para rodar o projeto rode o seguinte comando

    python manage.py runserver
    
Para disponibilizar o server (local) para outras pessoas dentro da mesma rede
é possivel passar parametros para o runserver, o parametro abaixo habilita
o uso do server no seu ip na rede (`ifconfig{`(linux), `ipconfig`(windows) para
visualizar o seu ip)

    python manage.py runserver 0.0.0.0:8000
    
Acesse o servidor romando na url `http://localhost:8000/`

## 8- Deploy

Para realizar o deploy será necessario adicionar um novo
remote (link que aponta para o repositorio) no seu projeto

No modelo do git é possivel ter multiplos remotes

O remote padrão do seu projeto é chamado de **origin** e no
nosso caso aponta para o repositorio do gitlab, agora precisamos
criar um que aponte para o servidor do openshift

Para isso iremos criar os seguintes comandos

1 - Para configurar o remote do ambiente de Dev

    git remote add develop ssh://584ca3bb2d5271189a0000a9@dev-weeget.rhcloud.com/~/git/dev.git/
    
2 - Para configurar o remote do ambiente de Produção

    git remote add master ssh://57dde79389f5cf2f180000bd@master-weeget.rhcloud.com/~/git/master.git/
    
Para verificar os remotes configurados no nosso projeto rodamos o
seguinte comando

    git remote -v
    
Será listado todos os remotes configurados `vale lembrar que remotes 
não são versionados, logo em toda maquina que for instalar o projeto
será necessario reconfigurar os remotes manualmente`

Para realiar alterações e commits no codigo, os pushs deverão ser
baseados nos respectivos remotes

1 - Local
    
    git push -u origin <branch>
    
2 - Dev
    
    git push -u develop develop
    
3 - Produção

    git push -u master master
    
**`A estrutura do projeto tem vinculada a branch MASTER com o ambiente
de produção, e a branch DEVELOP com o ambiente de Dev remote, commits
diferentes dessas no repositorio remoto não irá aplicar as alterações`**
    
## 9 - Open Shift

Para fazer uso da api completa do openshift é necessario autenticar a sua
maquina e dar as devidas permissões para a mesma, para isso é necessario
realizar algumas configurações adicionar

### Client Tools

O cliente tools é uma ferramenta para facilitar a configuração e permissoes
dos projetos da openshift

A ferramenta roda em um ambiente ruby, por isso e necessario instalar o ruby
e alguns pacotes de dependencias para roda-lo

1 - Instalando Ruby

    sudo apt-get install ruby-full
    
Para verificar se a instação ocorreu com sucesso o output do comando abaixo 
deverá ser `Welcome to Ruby`

    ruby -e 'puts "Welcome to Ruby"'
    
2 - Instalando pacotes dependentes

A Rubygem ja faz parte do pacote original do ruby, mas para algumas versões
mais antigas será necessario instalar o mesmo

    sudo apt-get install rubygems
    
3 - Instalando o Client Tools

    sudo gem install rhc
    
4 - Configurando sua Maquina Local

Para configurar sua maquina local e para ter as devidas permissões é necessario
fazer o logon no openshift com as credenciais do criador do projeto

    rhc setup -l claudio@weeget.com.br
    
**`Por motivos de segurança a senha que será solicitada será encontrada na wiki 
na pasta Acessos do  gitlab`**

Apos logar ele irá identificar se você possui uma **SSH KEY** (Aquela que criamos
anteriormente para utilizar o gitlab) e um Token de acesso, se tudo ocorreu bem 
nos passos anteriores voce ja deve possuir essa key, então ele irá ti perguntar
se deseja vincular essa key ao projeto do openshift, digite `sim` para a ssh key
e para o token

O token ti dará acesso ao uso da api (push, acesso remoto e etc...) e a ssh key
irá autenticar a sua maquina sem precisar digitar login e senha para cada
processo realizado no openshift

Apos esses passos você estará apto a realizar os deploys nos servidores de
produção(weeget.com.br) e desenvolvimento (dev-weeget.com.br)

### Scrips e Automatização

**Action Hookers**

Servem para executar scripts a cada fase do deploy (pre-build, build, pos-build,
deploy)

Dentro da pasta `.openshift/action_hooks/` estão os arquivos com os script
de configuração dos scripts

**Markers**

Marcadores são arquivos em branco que servem como flags para o openshift, se
ele encontrar o arquivo com esse nome dentro da pasta markers ele irá executar
a ação que representa o nome do arquivo (hot deploy e etc...)

**Trabalhando com hot deploy**

No diretório `.openshift/markers/` existe o arquivo hot_deploy que é 
responsável por não deixar o site cair durante o deploy.

**`Em algumas situações principalmente de mudança de relacionamento do BD
será necessários remover esse arquivo para que o server seja reiniciado
para reconhecer as alterações, as vezes nessa situação pode ocorrer do
site ficar off mesmo depois do deploy ser finalizado com sucesso, quando
acontecer basta entrar no console do openshift e reiniciar o serviço que
volta ao normal.`**

Para ver as variáveis de ambiente caso seja necessário a mudança:

    rhc env list -n weeget master

Para adicionar:

    rhc env set OPENSHIFT_PYTHON_WSGI_APPLICATION=wsgi/wsgi.py --app django



