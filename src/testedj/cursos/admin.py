from django.contrib import admin

# Register your models here.
from testedj.cursos.models import Curso

admin.site.register(Curso)
